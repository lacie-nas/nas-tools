# sd_alias

## Introduction

On multi-disk devices, there is no warranty about the disk node names.
For example, on a given system the node names for disk {0,1} could be
/dev/sd{a,b}. On the same system, with an extra eSATA or USB disk plugged, the
same disks could hold very different node names. This can be quite confusing
for the userspace applications.

On NAS devices, there is an additional constraints: a way must be available to
associate a disk with a casing slot. For example, if a disk is defective, the
corresponding fail LED must be set up. One will have to translate a disk node
name into a slot or a LED number.

Example: `/dev/sdb` => `/sys/class/leds/net5big-v2:blue:sata4`

On a given device, each SATA disk have an unique and constant SCSI ID. This ID
can be used to create some helpful aliases.

## What is sd_alias ?

sd_alias is an udev script which can be used to create SCSI disks aliases.
An alias is associated with a single SCSI ID. Aliases are stored under
`/dev/disk/by-alias/`. Each alias is a symlink pointing the real device node.

SCSI disk aliases example on a 5Big Network v2:

    # ls -l /dev/disk/

## How to configure sd_alias ?

Configuration must be stored in the /etc/sd_alias.conf file.
Each sd_alias.conf line must respect the syntax "${scsi_id} ${alias_name}".
The SCSI ID format is "host:channel:id:lun"

Configuration file example for a 5Big Network v2:

    0:3:0:0 sata0
    0:2:0:0 sata1
    0:1:0:0 sata2
    1:2:0:0 sata3
    1:1:0:0 sata4
    
Some possible configurations files for the Kirkwood based LaCie NASes are
available under the "conf" repository in sd_alias sources.

## How to install ?

Just go in the sd_alias source repository and do

    make install

and add a `/etc/sd_alias.conf` file (refer to the previous section).
