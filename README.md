# nas-tools

The nas-tools project is a collection of NAS system utilities;

- gpio_ctrl    : a POSIX shell script which allows to configure GPIOs.
- input-eventd : a deamon which allows to monitor input devices.
- led_ctrl     : a POSIX shell script which allows to configure LEDs.
- sd_alias     : an udev plugin which can be used to create SCSI disks aliases.

For more informations, please refer to the sub-projects README.

