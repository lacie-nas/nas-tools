SUBDIRS = sd_alias led_ctrl gpio_ctrl input-eventd

all check install uninstall clean :
	@(for subdir in $(SUBDIRS); do	\
	  $(MAKE) -C $$subdir $@;	\
	done)
