/*
 * This file is part of input-eventd.
 *
 * Copyright (C) 2011 Simon Guinot <simon.guinot@sequanux.org>
 *
 * input-eventd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * input-eventd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with input-eventd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <linux/input.h>
#include "input-event.h"
#include "debug.h"

/*
 * Event polling.
 */

static void evpoll_handler(int signum, siginfo_t *si, void *unused);

struct evpoll *alloc_evpoll(void)
{
	struct evpoll *evp;
	struct sigaction sa;
	struct sigevent sev;
	sigset_t mask;

	evp = (struct evpoll *) calloc(1, sizeof(*evp));
	if (!evp) {
		fprintf(stderr, "malloc: %s\n", strerror(errno));
		return NULL;
	}

	/* FIXME: get a real-time signal number */
	evp->signum = SIGRTMIN;

	/* Establish handler for timer signal */
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = evpoll_handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(evp->signum, &sa, NULL) == -1) {
		fprintf(stderr, "sigaction: %s\n", strerror(errno));
		goto err_free;
	}

	/* Block timer signal */
	sigaddset(&mask, evp->signum);
	if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1) {
		fprintf(stderr, "sigprocmask: %s\n", strerror(errno));
		goto err_free;
	}

	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = evp->signum;
	sev.sigev_value.sival_ptr = evp;
	if (timer_create(CLOCK_REALTIME, &sev, &evp->timerid) == -1) {
		fprintf(stderr, "timer_create: %s\n", strerror(errno));
		goto err_free;
	}

	return evp;

err_free:
	free(evp);
	return NULL;
}

void free_evpoll(struct evpoll *evp)
{
	if (evp->progname)
		free(evp->progname);
	free(evp);
}

int set_evpoll_progname(struct evpoll *evp, const char *progname)
{
	evp->progname = strdup(progname);
	if (!evp->progname) {
		fprintf(stderr, "strdup: %s\n", strerror(errno));
		return -1;
	}
	return 0;
}

static bool
have_evpoll_typecode(struct evpoll *evp, uint16_t type, uint16_t code)
{
	return test_bit(evp->types, type) && test_bit(evp->codes[type], code);
}

/*
 * Events handling.
 */

#define DEV_INPUT_EVENT "/dev/input"
#define EVENT_DEV_NAME "event"

static int device_open(const char *fname)
{
	int fd, version;

	fd = open(fname, O_RDONLY);
	if (fd == -1) {
		fprintf(stderr, "open %s: %s\n", fname, strerror(errno));
		return -1;
	}
	if (ioctl(fd, EVIOCGVERSION, &version) == -1) {
		fprintf(stderr, "ioctl EVIOCGVERSION: %s\n", strerror(errno));
		close(fd);
		return -1;
	}
	if (version != EV_VERSION) {
		fprintf(stderr,
			"protocol version mismatch (expected %d, got %d)\n",
			EV_VERSION, version);
		close(fd);
		return -1;
	}
	return fd;
}

static void device_info(int fd)
{
}

static bool device_match_evpoll(int fd, struct evpoll *evp)
{
	int i, ret;
	uint32_t type_bits[(EV_CNT >> 5) + 1];
	uint32_t code_bits[(KEY_CNT >> 5) + 1];

	ret = ioctl(fd, EVIOCGBIT(0, EV_MAX), type_bits);
	if (ret < 0) {
		fprintf(stderr, "ioctl EVIOCGBIT(0, %d): %s\n",
			EV_MAX, strerror(errno));
		return false;
	}

	for (i = 0; i < EV_CNT; i++) {
		int j;

		if (!test_bit(type_bits, i) || !test_bit(evp->types, i))
			continue;

		ret = ioctl(fd, EVIOCGBIT(i, KEY_MAX), code_bits);
		if (ret < 0) {
			fprintf(stderr, "ioctl EVIOCGBIT(%d, %d): %s\n",
				i, KEY_MAX, strerror(errno));
			return false;
		}
		for (j = 0; j < KEY_CNT; j++)
			if (test_bit(code_bits, j) && test_bit(evp->codes[i], j))
				return true;
	}

	return false;
}

static bool device_match_list(int fd, struct list_head *list)
{
	struct evpoll *evp;

	list_for_each_entry(evp, list, list)
		if (device_match_evpoll(fd, evp))
			return true;

	return false;
}

static int is_event_device(const struct dirent *dir)
{
        return strncmp(EVENT_DEV_NAME, dir->d_name, 5) == 0;
}

static int run_program(const char *progname, struct input_event *iev)
{
	long long ctime, ltime;
	char val_arg[32], ctime_arg[32], ltime_arg[32];
	char *argv[5];

	ctime = ((long long) iev[EV_CURR].time.tv_sec * 1000) +
			((long long) iev[EV_CURR].time.tv_usec / 1000);
	ltime = ((long long) iev[EV_LAST].time.tv_sec * 1000) +
			((long long) iev[EV_LAST].time.tv_usec / 1000);

	snprintf(val_arg, sizeof(val_arg), "%ld", (long) iev[EV_CURR].value);
	snprintf(ctime_arg, sizeof(ctime_arg), "%lld", ctime);
	snprintf(ltime_arg, sizeof(ltime_arg), "%lld", ltime);

	argv[0] = (char *) progname;
	argv[1] = val_arg;
	argv[2] = ctime_arg;
	argv[3] = ltime_arg;
	argv[4] = NULL;

	dbg("Run %s %s %s %s", progname, val_arg, ctime_arg, ltime_arg);

	switch (fork()) {
	case -1:
		fprintf(stderr, "timer_settime: %s\n", strerror(errno));
		return -1;
	case 0:
		execvp(progname, argv);
		fprintf(stderr, "execv: %s\n", strerror(errno));
		exit(-1);
	}

	return 0;
}

static void evpoll_handler(int signum, siginfo_t *si, void *unused)
{
	struct evpoll *evp = si->si_value.sival_ptr;

	run_program(evp->progname, evp->iev);
}

static int handle_input_event(struct input_event *iev, struct list_head *list)
{
	struct evpoll *evp;
	struct itimerspec its;

	dbg("Event %ld.%ld type=0x%04x code=0x%04x value=%d",
		iev->time.tv_sec, iev->time.tv_usec,
		iev->type, iev->code, iev->value);

	list_for_each_entry(evp, list, list) {
		if (!have_evpoll_typecode(evp, iev->type, iev->code))
			continue;

		/* FIXME: lock evp data */
		evp->iev[EV_LAST] = evp->iev[EV_CURR];
		evp->iev[EV_CURR] = *iev;
		if (!evp->debounce_delay) {
			run_program(evp->progname, evp->iev);
			return 0;
		}

		/* Defer input event handling */
		its.it_value.tv_sec = evp->debounce_delay / 1000;
		its.it_value.tv_nsec = (evp->debounce_delay % 1000) * 1000000;
		its.it_interval.tv_sec = 0;
		its.it_interval.tv_nsec = 0;

		if (timer_settime(evp->timerid, 0, &its, NULL) == -1) {
			fprintf(stderr, "timer_settime: %s\n", strerror(errno));
			return -1;
		}
	}

	return 0;
}

int input_event_poll(struct list_head *list)
{
	struct dirent **namelist;
	int i, ndev;
	int *fds = NULL;
	int num_fds = 0;
	sigset_t mask;

	/* Unblock timer signals */
	if (sigprocmask(SIG_SETMASK, NULL, &mask) == -1 ||
		sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1) {
		fprintf(stderr, "sigprocmask: %s\n", strerror(errno));
		return -1;
	}

	ndev = scandir(DEV_INPUT_EVENT, &namelist, is_event_device, alphasort);
	if (ndev <= 0)
		return ndev;

	for (i = 0; i < ndev; i++) {
		int fd;
		char fname[64];

		snprintf(fname, sizeof(fname), "%s/%s",
				DEV_INPUT_EVENT, namelist[i]->d_name);
		fd = device_open(fname);
		if (fd < 0)
			continue;

		free(namelist[i]);

		if (!device_match_list(fd, list)) {
			close(fd);
			continue;
		}

		fds = (int *) realloc(fds, (num_fds + 1) * sizeof(int));
		if (fds == NULL) {
			fprintf(stderr, "malloc: %s\n", strerror(errno));
			goto err;
		}

		/* Add input event device to the poll list. */
		fds[num_fds] = fd;
		num_fds += 1;

		/* Display some fancy description about this device. */
		device_info(fd);
        }

	if (!num_fds) {
		fprintf(stderr, "no input-event device found\n");
		goto err;
	}

	/*
	 * Polling loop.
	 */
        for (;;) {
		int fd_max = 0;
		fd_set set;

                FD_ZERO(&set);

		for (i = 0; i < num_fds; i++) {
			FD_SET(fds[i], &set);
			fd_max = (fds[i] < fd_max) ? fd_max : fds[i];
		}

                switch (select(fd_max + 1, &set, NULL, NULL, NULL)) {
			case -1:
				if (errno == EAGAIN || errno == EINTR)
					continue;
				fprintf(stderr, "select: %s\n", strerror(errno));
				goto err;
			case 0:
				fprintf(stderr, "select return 0 ?!?\n");
				goto err;
		}

		for (i = 0; i < num_fds; i++) {
			struct input_event iev;

			if (!FD_ISSET(fds[i], &set))
				continue;

			switch (read(fds[i], &iev, sizeof(iev))) {
			case -1:
				if (errno == EAGAIN || errno == EINTR)
					continue;
				fprintf(stderr, "read: %s\n", strerror(errno));
				goto err;
			case 0:
				fprintf(stderr, "EOF\n");
				goto err;
			}

			handle_input_event(&iev, list);
		}
        }

err:
	free(fds);
	return -1;
}
