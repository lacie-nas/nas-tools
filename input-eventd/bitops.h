/*
 * This file is part of input-eventd.
 *
 * input-eventd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * input-eventd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with input-eventd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BITOPS_H
#define BITOPS_H

#include <stdbool.h>
#include <stdint.h>

#define BIT_MASK(nr)	(1UL << ((nr) % 32))
#define BIT_WORD(nr)	((nr) / 32)

static inline bool test_bit(uint32_t *map, int nr)
{
        return !!(map[BIT_WORD(nr)] & BIT_MASK(nr));
}

static inline void set_bit(uint32_t *map, int nr)
{
	map[BIT_WORD(nr)] |= BIT_MASK(nr);
}

static inline void clear_bit(uint32_t *map, int nr)
{
	map[BIT_WORD(nr)] &= ~BIT_MASK(nr);
}

#endif
