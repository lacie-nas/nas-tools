/*
 * This file is part of input-eventd.
 *
 * Copyright (C) 2011 Simon Guinot <simon.guinot@sequanux.org>
 *
 * input-eventd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * input-eventd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with input-eventd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>
#include "config.h"
#include "input-event.h"

extern bool debug;

#ifdef HAVE_GETOPT_LONG
#define _GNU_SOURCE
#include <getopt.h>

static const struct option long_options[] =
{
	{"daemon", 0, 0, 'd'},
	{"file", 1, 0, 'f'},
	{"help", 0, 0, 'h'},
	{"verbose", 0, 0, 'v'},
	{0, 0, 0, 0}
};
#endif

static const char *short_options = "df:hv";

static void usage(void)
{
	fprintf(stdout, "Usage : input-eventd [OPTIONS...]\n");
#ifdef HAVE_GETOPT_LONG
	fprintf(stdout, "\t-d, --daemon  : detach and run in background\n");
	fprintf(stdout, "\t-f, --file    : configuration file\n");
	fprintf(stdout, "\t-h, --help    : display this help\n");
	fprintf(stdout, "\t-v, --verbose : enable debug traces\n");
#else
	fprintf(stdout, "\t-d : detach and run in background\n");
	fprintf(stdout, "\t-f : configuration file\n");
	fprintf(stdout, "\t-h : display this help\n");
	fprintf(stdout, "\t-v : enable debug traces\n");
#endif
}

int main(int argc, char *argv[])
{
	LIST_HEAD(evpoll_list);
	bool daemonize = false;
	char *conffile = NULL;
	int opt;
#ifdef HAVE_GETOPT_LONG
	int option_index = 0;

	while ((opt = getopt_long(argc, argv, short_options, long_options,
					&option_index)) != EOF)
#else
	while ((opt = getopt(argc, argv, short_options)) != EOF)
#endif
	{
		switch (opt) {
		case 'd': /* --daemon */
			daemonize = true;
			break;
		case 'f': /* --file */
			conffile = optarg;
			break;
		case 'h': /* --help */
			usage();
			return 0;
		case 'v': /* --verbose */
			debug = true;
			break;
		default:
			return -1;
		}
	}

	if (!conffile) {
		usage();
		return 0;
	}

	if (daemonize && daemon(0, 1) == -1) {
		fprintf(stderr, "daemon: %s\n", strerror(errno));
		return -1;
	}

	if (parse_config(conffile, &evpoll_list) == -1)
		return -1;
	if (list_empty(&evpoll_list)) {
		fprintf(stderr, "%s: no input events found\n", conffile);
		return -1;
	}

	return input_event_poll(&evpoll_list);
}
