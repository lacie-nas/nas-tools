/*
 * This file is part of input-eventd.
 *
 * Copyright (C) 2011 Simon Guinot <simon.guinot@sequanux.org>
 *
 * input-eventd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * input-eventd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with input-eventd.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * - Configuration file example:
 *
 * type=EV_KEY; code=KEY_POWER; debounce_delay=1000; run=/bin/key
 * type=EV_SW; debounce_delay=1000; run=/bin/sw
 * run=/bin/dbg; invalid=invalid # Some comments
 *
 * - Convert each configuration file line into a cfg_token linked list:
 *
 * ,______________,   ,______________,   ,_____________,   ,______________,
 * |  cfg_token   |-->|  cfg_token   |-->|  cfg_token  |-->|  cfg_token   |
 * |--------------|   |--------------|   |-------------|   |--------------|
 * |key: type     |   |key: code     |   |key: debounce|   |key: run      |
 * |val: EV_KEY   |   |val: KEY_POWER|   |val: 1000    |   |val: /bin/prog|
 * `--------------'   `--------------'   `-------------'   `--------------'
 *
 * ,______________,   ,_____________,   ,______________,
 * |  cfg_token   |-->|  cfg_token  |-->|  cfg_token   |
 * |--------------|   |-------------|   |--------------|
 * |key: type     |   |key: debounce|   |key: run      |
 * |val: EV_SW    |   |val: 1000    |   |val: /bin/sw  |
 * `--------------'   `-------------'   `--------------'
 *
 * ,______________,   ,_____________,
 * |  cfg_token   |-->|  cfg_token  |
 * |--------------|   |-------------|
 * |key: run      |   |key: invalid |
 * |val: /bin/dbg |   |val: invalid |
 * `--------------'   `-------------'
 *
 * - Build an evpoll linked list:
 *
 * ,_____________________,   ,_____________________,   ,_____________________,
 * |       evpoll        |-->|       evpoll        |-->|       evpoll        |
 * |---------------------|   |---------------------|   |---------------------|
 * |prognam  : /bin/key  |   |prognam  : /bin/sw   |   |prognam  : /bin/dbg  |
 * |debounce : 1000      |   |debounce : 1000      |   |debounce : 0         |
 * |types    : EV_KEY    |   |types    : EV_SW     |   |types    : all       |
 * |codes    : KEY_POWER |   |codes    : all       |   |codes    : all       |
 * `---------------------'   `---------------------'   `---------------------'
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>

#include "list.h"
#include "input-event.h"
#include "input-tables.h"
#include "debug.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

struct cfg_token {
	const char *key;
	const char *value;
	struct list_head list;
};

/*
 * Remove leading/trailing white-space and control characters.
 */
static char *strip(char *token)
{
	int len = strlen(token);

	while (len-- && (iscntrl(token[len]) || isspace(token[len])))
		token[len] = '\0';

	while (len-- && (iscntrl(*token) || isspace(*token)))
		token++;

	return token;
}

static struct cfg_token *
get_cfg_token_next(const char *key, struct list_head *list,
			struct cfg_token *cfg)
{
        cfg = list_prepare_entry(cfg, list, list);
	list_for_each_entry_continue(cfg, list, list)
		if (strcmp(key, cfg->key) == 0)
			return cfg;

	return NULL;
}

static struct cfg_token *init_cfg_token(char *token)
{
	struct cfg_token *cfg;
	char *key = token;
	char *value;

	value = strchr(token, '=');
	if (!value)
		return NULL;
	*value++ = '\0';

	cfg = (struct cfg_token *) calloc(1, sizeof(struct cfg_token));
	if (!cfg) {
		fprintf(stderr, "calloc: %s\n", strerror(errno));
		return NULL;
	}

	cfg->key = strip(key);
	cfg->value = strip(value);

	dbg("config token [%s=%s]", cfg->key, cfg->value);

	return cfg;
}

/*
 * Convert a string into an event code number. A valid string should be:
 * - an event type definition as found in linux/input.h.
 *   Example: "EV_KEY", "EV_SW", "EV_PWR", etc...
 * - or a numeric string (base 16 or 10)
 */
static int string_to_type(const char *str, uint16_t *type)
{
	char *end;
	int i;

	/* Get event type from linux/input.h definition */
	for (i = 0; i < ARRAY_SIZE(event_types); i++)
		if (strcmp(str, event_types[i].string) == 0) {
			*type = event_types[i].value;
			return 0;
		}

	/* Get event type from a numeric string (base 16 or 10) */
	*type = strtoul(str, &end, 16);
	if (errno || *end)
		*type = strtoul(str, &end, 10);
	if (errno || *end)
		goto err;

	/* Check converted event code number */
	for (i = 0; i < ARRAY_SIZE(event_types); i++)
		if (event_types[i].value == *type)
			return 0;

err:
	fprintf(stderr, "invalid event type: %s\n", str);

	return -1;
}

/*
 * Convert a string into an event code number. A valid string should be:
 * - an event code definition as found in linux/input.h.
 *   Example: "KEY_POWER", "KEY_SLEEP", "KEY_WAKEUP", etc...
 * - or a numeric string (base 16 or 10)
 */
static int string_to_code(const char *str, uint16_t type, uint16_t *code)
{
	struct input_entry *ev_codes = NULL;
	uint16_t ev_codes_count = 0;
	char *end;
	int i;

	/* Get event codes table from type */
	for (i = 0; ARRAY_SIZE(table_index); i++)
		if (table_index[i].type == type) {
			ev_codes = table_index[i].codes;
			ev_codes_count = table_index[i].count;
			break;
		}
	if (!ev_codes)
		goto err;

	/* Get event code from linux/input.h definition */
	for (i = 0; i < ev_codes_count; i++)
		if (strcmp(str, ev_codes[i].string) == 0) {
			*code = ev_codes[i].value;
			return 0;
		}

	/* Get event code from a numeric string (base 16 or 10) */
	*code = strtoul(str, &end, 16);
	if (errno || *end)
		*code = strtoul(str, &end, 10);
	if (errno || *end)
		goto err;

	/* Check converted event code number */
	for (i = 0; i < ev_codes_count; i++)
		if (ev_codes[i].value == *code)
			return 0;

err:
	fprintf(stderr, "invalid event code: type=%d code=%s\n", type, str);

	return -1;
}

static struct evpoll *build_evpoll(struct list_head *list)
{
	struct evpoll *evp;
	struct cfg_token *cfg;
	uint16_t type;

	evp = alloc_evpoll();
	if (!evp)
		return NULL;

	/* Set event debounce delay */
	cfg = get_cfg_token_next("debounce_delay", list, NULL);
	if (cfg)
		set_evpoll_debounce_delay(evp, atoi(cfg->value));

	/* Set event program name */
	cfg = get_cfg_token_next("run", list, NULL);
	if (cfg)
		set_evpoll_progname(evp, cfg->value);

	/* Set event type */
	cfg = get_cfg_token_next("type", list, NULL);
	if (!cfg) {
		set_evpoll_type_all(evp);
		return evp;
	}
	if (string_to_type(cfg->value, &type) == -1)
		goto err;
	set_evpoll_type(evp, type);

	/* Set event code */
	cfg = get_cfg_token_next("code", list, NULL);
	if (!cfg) {
		set_evpoll_typecode_all(evp, type);
		return evp;
	}
	do {
		uint16_t code;

		if (string_to_code(cfg->value, type, &code) == -1)
			goto err;
		set_evpoll_typecode(evp, type, code);
	} while ((cfg = get_cfg_token_next("code", list, cfg)));

	return evp;

err:
	free_evpoll(evp);
	return NULL;
}

int parse_config(const char *conffile, struct list_head *evpoll_list)
{
	int err = 0;
	FILE *file;
	char line[512];

	file = fopen(conffile, "r");
	if (!file) {
		fprintf(stderr, "fopen %s: %s\n", conffile, strerror(errno));
		return -1;
	}

	/* FIXME: handle line length over sizeof(line) */
	while (fgets(line, sizeof(line), file)) {
		LIST_HEAD(config_list);
		struct cfg_token *cfg, *tmp;
		struct evpoll *evp;
		char *token = line;
		char *end;

		dbg("config line  [%s]", line);

		/* Discard trailing comments */
		end = strchr(token, '#');
		if (end)
			*end = '\0';

		/* Convert configuration line into a cfg_token linked list */
		while (token) {
			char *delim = strchr(token, ';');

			if (delim)
				*delim = '\0';

			cfg = init_cfg_token(token);
			if (!cfg) {
				fprintf(stderr, "invalid token [%s]\n", token);
				err = -1;
				goto free_config_list;
			}

			list_add(&cfg->list, &config_list);
			if (delim)
				token = delim + 1;
			else
				token = NULL;
		}

		/* Convert the cfg_token linked list into evpoll */
		evp = build_evpoll(&config_list);
		if (evp)
			insert_evpoll(evp, evpoll_list);

free_config_list:
		list_for_each_entry_safe(cfg, tmp, &config_list, list) {
			list_del(&cfg->list);
			free(cfg);
		}
		if (err)
			break;
	};

	return err;
}
