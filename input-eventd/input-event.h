/*
 * This file is part of input-eventd.
 *
 * Copyright (C) 2011 Simon Guinot <simon.guinot@sequanux.org>
 *
 * input-eventd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * input-eventd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with input-eventd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INPUT_EVENT_H
#define INPUT_EVENT_H

#include <linux/input.h>

#include "list.h"
#include "bitops.h"

#define EV_LAST	0
#define EV_CURR	1

struct evpoll {
	uint32_t		types[BIT_WORD(EV_CNT) + 1];
	uint32_t		codes[EV_CNT][BIT_WORD(KEY_CNT) + 1];
	int			debounce_delay;
	char			*progname;
	timer_t			timerid;
	int			signum;
	struct input_event	iev[2];
	struct list_head 	list;
};

static inline void insert_evpoll(struct evpoll *evp, struct list_head *list)
{
	list_add(&evp->list, list);
}

static inline void delete_evpoll(struct evpoll *evp)
{
	list_del(&evp->list);
}

static inline void
set_evpoll_typecode(struct evpoll *evp, uint16_t type, uint16_t code)
{
	set_bit(evp->types, type);
	set_bit(evp->codes[type], code);
}

static inline void
set_evpoll_typecode_all(struct evpoll *evp, uint16_t type)
{
	int i;

	set_bit(evp->types, type);
	for (i = 0; i < KEY_CNT; i++)
		set_bit(evp->codes[type], i);
}

static inline void set_evpoll_type(struct evpoll *evp, uint16_t type)
{
	set_bit(evp->types, type);
}

static inline void set_evpoll_type_all(struct evpoll *evp)
{
	int i;

	for (i = 0; i < EV_CNT; i++)
		set_evpoll_typecode_all(evp, i);
}

static inline void
set_evpoll_debounce_delay(struct evpoll *evp, int debounce_delay)
{
	evp->debounce_delay = debounce_delay;
}

struct evpoll *alloc_evpoll(void);
void free_evpoll(struct evpoll *evp);
int set_evpoll_progname(struct evpoll *evp, const char *progname);

int input_event_poll(struct list_head *list);

#endif
