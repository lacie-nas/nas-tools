# -*- coding: utf-8 -*-
# This file is part of the lacie-nas project
#
# Copyright 2011 Benoît Canet
#
# Authors:
# - Benoît Canet <benoit.canet@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import string
import re

def is_hex(value):
	try:
		int(value, 16)
		return True
	except:
		return False

def is_int(value):
	try:
		int(value)
		return True
	except:
		return False

def is_string(value):
	for i in value:
		if not i in string.ascii_letters + "_":
			return False
	return True


def check_name_and_value(name, value):
	if not is_hex(value) and not is_int(value):
		return False
	if not is_string(name):
		return False
	return True

def strip_comments(line):
	line = re.sub("/\*.*\*/", "", line)
	return line

def parse_line(line):
	line = strip_comments(line)
	result = {}
	_, name, value = line.strip().split(None, 2)
	if name == "EV_VERSION":
		return result
	if check_name_and_value(name, value):
		result[value] = name
	return result

def parse_file_by_prefix(filepath, prefix):
	result = {}
	fd = open(filepath, "r")
	line = fd.readline()
	while line:
		if line.startswith("#define") and \
		   line.find(prefix) != -1:
			result.update(parse_line(line))	
		line = fd.readline()
	fd.close()

	return result

def get_event_type_list(array):
	result = []

	for key in array:
		value = array[key]
		result.append(value[3:])
	
	return result
