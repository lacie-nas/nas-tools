#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of the lacie-nas project
#
# Copyright 2011 Benoît Canet
#
# Authors:
# - Benoît Canet <benoit.canet@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from gen import generate
from tests import run_test
import argparse
import sys

def main():
	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument('--filename',
			    help='Header to parse "linux/input.h"')
	parser.add_argument('--tests',
			    action='store_true',
			    help='Run unit tests')
	args = parser.parse_args()

	if args.tests == True:
		run_test()
	elif args.filename == None:
		parser.print_help()
		sys.exit(0)
	else:
		print generate(args.filename)

if __name__ == "__main__":
	main()
