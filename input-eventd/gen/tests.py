# -*- coding: utf-8 -*-
# This file is part of the lacie-nas project
#
# Copyright 2011 Benoît Canet
#
# Authors:
# - Benoît Canet <benoit.canet@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from gen import *
from parser import *
import unittest
import subprocess

class TestSuite(unittest.TestCase):

	def test_strip_comments(self):
		result = strip_comments("HELLO/* SDFDSFSD sdfsd */WORLD")
		self.assertEqual(result, "HELLOWORLD")

	def test_parse_line(self):
		test_array = { '0x55' :  "VALUE" }

		array = parse_line("#define VALUE 0x55")
		self.assertEqual(array, test_array)

		array = parse_line("#define VALUE      0x55")
		self.assertEqual(array, test_array)

		array = parse_line("#define VALUE_GOOD      0x55")
		test_array = { '0x55' :  "VALUE_GOOD" }
		self.assertEqual(array, test_array)

		array = parse_line("#define VALUE-BAD      0x55")
		self.assertEqual(array, {})

		array = parse_line("#define VALUE 0x55 3454")
		self.assertEqual(array, {})

		array = parse_line(" #define KEY_POWER 116 /* SC System Power Down */")
		test_array = { '116' :  "KEY_POWER" }
		self.assertEqual(array, test_array)

		array = parse_line("#define EV_VERSION              0x010001")
		self.assertEqual(array, {})

	def test_events(self):
		array = parse_file_by_prefix("test_parse_event.h", "EV_")

		test_array = { '0x69' : "EV_TEST",
			       '0x70' : "EV_TAISTE" }

		self.assertEqual(array, test_array)

	def test_get_event_type_list(self):
		array = parse_file_by_prefix("test_parse_event.h", "EV_")

		test_array = { '0x69' : "EV_TEST",
			       '0x70' : "EV_TAISTE" }

		result = get_event_type_list(test_array)

		self.assertEqual(result.sort(), ["TEST", "TAISTE"].sort())
	
	def test_generate_table(self):
		test_array = { '0x69' : "EV_TEST",
			       '0x70' : "EV_TAISTE" }
		
		test_string = """struct input_entry event_type[] = {
	{.value = 0x70, .string = "EV_TAISTE"},
	{.value = 0x69, .string = "EV_TEST"}
};

"""

		result = generate_table("event_type", test_array)
		self.assertEqual(result, test_string)

	def test_generate_index(self):
		test_array = { '0x69' : "EV_TEST",
			       '0x70' : "EV_TAISTE" }
		test_list  = [ "TEST" ]
		codes_count = { "codes_test" : 2}

		result = generate_index(test_array, test_list, codes_count)
		test_string = """struct index_entry table_index[] = {
	{.type = 0x69, .codes = codes_test, .count = 2}
};

"""
		self.assertEqual(result, test_string)

	def test_compilation(self):
		result = generate("/usr/include/linux/input.h")
		fd = open("/tmp/test_header.h", "w")
		fd.write(result)
		fd.close()

		result = subprocess.call(["gcc", "test_main.c",  \
					  "-I",  "/tmp", "-o", \
					  "/tmp/test"])
		
		self.assertEqual(result , 0)

		result = subprocess.call(["/tmp/test"])
		self.assertEqual(result, 0)

def run_test():
	suite = unittest.TestLoader().loadTestsFromTestCase(TestSuite)
	unittest.TextTestRunner(verbosity=2).run(suite)

if __name__ == '__main__':
    unittest.main()
