# led_ctrl

## What is led_ctrl ?

led_ctrl is a POSIX shell script which allow to configure LEDs (compatible
with the GNU/Linux sysfs LED API) via an understandable configuration file.

## How to install ?

Just go in the led_ctrl source repository and do

    make install

## How to use ?

Without any arguments, led_ctrl will list all the available modes:

    $ led_ctrl
    suspend
    startup
    shutdown
    update

To set startup mode:

    $ led_ctrl startup

## How to configure ?

After installation, the led_ctrl configuration file should be
`/etc/led_ctrl.conf`. An alternate configuration file can be specified using
the options `--config` or `-c`. Note that a default configuration file is
provided with this package. Read this file could be helpful.

A configuration file contain some LED mode definitions. A LED mode allow to
apply settings to one or several LEDs.

### Configuration file format:

    {mode_name1}
    led_name1	attr1=value1 attr2=value2 ...
    led_name2	attr1=value1 attr2=value2 ...
    ...
    
    {mode_name2}
    ...

### Configuration format rules:

* Comments and empty lines are supported.

* A Mode keyword must be enclosed in '{' '}'.

* Each mode settings line must begin with a LED name pattern. As wildcards
  and globbing are supported, the following settings could apply to several
  LEDs.

* Space and tabulations are the only valid separators between LED names and
  attribute settings.

* LED settings are of the form `attr=value`. Attribute names and values
  should be valid regarding the sysfs LED API.

### Mode examples:

    {off} # turn off all the LEDs
    *       brightness=0
    
    {startup} # turn on the power LED and enable SATA activity on the rear LEDS
    *:blue:power	trigger=none brightness=255
    *:blue:sata*	trigger=none sata=1 brightness=255
